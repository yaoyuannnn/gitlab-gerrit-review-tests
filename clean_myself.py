#!/usr/bin/env python

import argparse
from git import Repo
import re
import gitlab
import os


def clean_mrs(remote):
    m=re.match(r"git@(.*):(.*)\.git$", remote.url)
    if not m:
        raise Exception("Unhandled remote url format: {remote.url}")

    gitlab_host, project_path = m.groups()

    gl = gitlab.Gitlab(f"https://{gitlab_host}", private_token=os.getenv("GITLAB_PRIVATE_TOKEN"))
    proj = gl.projects.get(project_path)
    victims = []
    for mr in proj.mergerequests.list(state="opened", iterator=True):
        if re.match("(test|master)-.*$", mr.source_branch):
            victims.append(mr)

    if len(victims) == 0:
        print("No test MRs to close.")
        return

    print(f"Closing {len(victims)} test MRs...")

    for mr in victims:
        mr.state_event = "close"
        mr.save()

    print("MRs cleaned.")
    

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--remote", default="origin")
    args = parser.parse_args()

    remote = args.remote

    repo = Repo(".")

    clean_mrs(repo.remotes[remote])
    
    repo.remotes[remote].fetch(prune=True)
    victims = []
    for ref in repo.remotes[remote].refs:
        name = ref.remote_head
        m = re.match("(test|master)-.*$", name)
        if m:
            print(f"Will delete branch {m[0]}")
            victims.append(m[0])

    if not victims:
        print("Branches all clean")
    else:
        print(f"Deleting {len(victims)} branches...")
        repo.git.push(remote, victims, delete=True)


if __name__ == "__main__":
    main()
